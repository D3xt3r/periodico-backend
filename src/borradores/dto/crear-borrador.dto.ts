export class CrearBorradorDto {
  readonly titulo: string
  readonly contenido: string
  readonly categoria: string
}