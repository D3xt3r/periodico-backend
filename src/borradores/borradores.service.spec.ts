import { Test, TestingModule } from '@nestjs/testing';
import { BorradoresService } from './borradores.service';

describe('BorradoresService', () => {
  let service: BorradoresService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BorradoresService],
    }).compile();

    service = module.get<BorradoresService>(BorradoresService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
